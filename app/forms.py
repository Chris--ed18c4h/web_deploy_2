from sqlalchemy.sql.sqltypes import Boolean

from flask_wtf import Form
from wtforms import IntegerField
from wtforms import TextField
from wtforms import DateField
from wtforms import BooleanField
from wtforms.validators import DataRequired

class BookForm(Form):
    title = TextField('Book Title', validators=[DataRequired()])
    author = TextField('Author', validators=[DataRequired()])
    genre = TextField('Genre', validators=[DataRequired()])
    publish = DateField('Published', format='%d-%m-%Y',validators=[DataRequired()])
    

class UserForm(Form):
    name = TextField('name', validators=[DataRequired()])
    password = TextField('password', validators=[DataRequired()])

class LoginForm(Form):
    name = TextField('name', validators=[DataRequired()])
    password = TextField('password', validators=[DataRequired()])