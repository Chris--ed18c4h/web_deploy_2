from flask import render_template, flash, request
from app import app
from .forms import BookForm
from .forms import UserForm
from app import db, models
import logging
 
logging.basicConfig(filename = 'website.log', level=logging.INFO)

import datetime
from flask import redirect
from flask_login import LoginManager,login_required, current_user,login_user, logout_user



#TODO
#Add way to delete
#add sepereate page for other perosn
#finish forms
#change date text to reflect real

#--------------------------------------------------

@app.route('/logout')
def logout():
    logout_user()
    return redirect('/')
#----------------------------------------------
@app.route('/view_My/<id>', methods=['GET', 'POST'])
@login_required
def view_user_books(id):	
    user = models.User.query.get(id)
    if user is None:
        logging.error('user was not found!')
    return render_template('view_My.html',title = "add_book",user_books = user.books, userName = user.username)
#-----------------------------------------
@app.route('/view_My', methods=['GET', 'POST'])
@login_required
def view_My():	
    user = models.User.query.filter_by(username = current_user.username).first()
    if user is None:
        logging.error('user was not found!')
    
    return render_template('view_My.html',title = "add_book",user_books = user.books, userName = user.username)
#------------------------------------------
@app.route('/view_all/<id>', methods=['GET'])
@login_required
def view_user(id):
    book  = models.Books.query.get(id)
    if book is None:
        logging.error('book was not found!')
    return render_template('view_all.html', title = "view user", all_users = book.books, bookName = book.title)
#------------------------------------------
@app.route('/add_book_to_user/<id>', methods=['GET'])
@login_required
def append_book(id):
    book  = models.Books.query.get(id)
    user = models.User.query.filter_by(username = current_user.username).first()
    user.books.append(book)
    book.books.append(user)
    db.session.commit()
    logging.info('%s added a book!', current_user.username)
    return redirect('/view_b')
#------------------------------------------
@app.route('/settings', methods=['GET', 'POST'])
@login_required
def settings():
    form = UserForm()
    if form.validate_on_submit():
        password = form.name.password
        
        user = models.User.query.filter_by(username = current_user.username).first()
        user.set_password(password)
        db.session.commit()
        logging.info('%s changed the password', current_user.username)

    return render_template('settings.html', form=form)

#---------------------------------------------
@app.route('/', methods=['GET', 'POST'])
def index():
  return render_template('welc.html')
#-----------------------------------------------
@app.route('/view_all', methods=['GET', 'POST'])
@login_required
def view_all():	
    all_users = models.User.query.all()
    return render_template('view_all.html',title = "add_book",all_users = all_users)
#-----------------------------------------------

#-----------------------------------------------
@app.route('/view_b', methods=['GET', 'POST'])
@login_required
def view_books():	
    all_books= models.Books.query.all()

    return render_template('view_b.html',title = "add_book",all_books = all_books)
#-----------------------------------------------
@app.route('/add_b', methods=['GET', 'POST'])
@login_required
def add_b():	
    form = BookForm()
    if form.validate_on_submit():
        form_title = form.title.data
        form_author = form.author.data
        form_genre = form.genre.data
        
        form_publis = form.publish.data


        a = models.Books(bookID = form_title + form_author,
        title = form_title,
        author= form_author,
        genre = form_genre,
        pages = 50, 
        publish = form_publis )

        db.session.add(a)
        db.session.commit()
        logging.info('%s created a new book', form.title.data)
        flash('Succesfully received form data. %s  %s   %s'%(form.title.data,form.author.data,form.publish.data))
        

    return render_template('add_b.html',title = "add_book",form = form)
#-------------------------------------------------
@app.route('/login', methods = ['POST', 'GET'])
def login():
    if current_user.is_authenticated:
        return redirect('/')
    
    form =UserForm()
    if form.validate_on_submit():

        user = models.User.query.filter_by(username = form.name.data).first()
        if user is not None and user.check_password(form.password.data):
            login_user(user)
            logging.info('%s account login', form.name.data)
            return redirect('/')
     
    return render_template('login.html', form = form)
#-------------------------------------------------
@app.route('/reg', methods=['POST', 'GET'])
def register():
    if current_user.is_authenticated:
        return redirect('/')

    form =UserForm()
    if form.validate_on_submit():
        
        username = form.name.data
        password = form.password.data

        user = models.User(username=username)
        user.set_password(password)
        
        db.session.add(user)
        db.session.commit()
        logging.info('%s account created', form.name.data)
        return redirect('/login')
        
    return render_template('register.html', form = form)

