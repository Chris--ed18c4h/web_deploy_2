from app import db
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash



book_relations = db.Table('book_relations', db.Model.metadata,
    db.Column('id', db.String, db.ForeignKey('user.id')),
    db.Column('bookID', db.String, db.ForeignKey('books.bookID'))
)


class User(UserMixin,db.Model):

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80))
    password = db.Column(db.String(80))
    location = db.Column(db.String(80))
    authenticated = db.Column(db.Boolean, default=False)
    books = db.relationship('Books', secondary=book_relations)

    def set_password(self,password):
        self.password = generate_password_hash(password)
     
    def check_password(self,password_form):
        return check_password_hash(self.password,password_form)




class Books(db.Model):

    bookID = db.Column(db.String, primary_key=True)
    title = db.Column(db.String(80))
    author = db.Column(db.String(80))
    genre = db.Column(db.String(80))
    publish = db.Column(db.Date)
    pages = db.Column(db.Integer)
    books = db.relationship('User', secondary=book_relations, overlaps='books')