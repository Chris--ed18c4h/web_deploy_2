import os
import unittest
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from app import app, db, models

class TestCase(unittest.TestCase):
    def setUp(self):
        app.config.from_object('config')
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        #the basedir lines could be added like the original db
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
        self.app = app.test_client()
        db.create_all()
        pass



    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_route0(self):
        response = self.app.get('/',
                               follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_route1(self):
        response = self.app.get('/add_b',
                               follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_route2(self):
        response = self.app.get('/view_My',
                               follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_route3(self):
        response = self.app.get('/view_b',
                               follow_redirects=True)
        self.assertEqual(response.status_code, 200)  

    def test_route4(self):
        response = self.app.get('/login',
                               follow_redirects=True)
        self.assertEqual(response.status_code, 200)  

    def test_route5(self):
        response = self.app.get('/reg',
                               follow_redirects=True)
        self.assertEqual(response.status_code, 200) 



if __name__ == "__main__":
    unittest.main()